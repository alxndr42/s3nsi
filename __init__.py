import math
import random

from ctx import Context
import leds
from st3m.application import Application, ApplicationContext
from st3m.input import InputState, PetalState, Pressable
from st3m.ui.elements.menus import SimpleMenu
from st3m.ui.menu import MenuItemLaunchPersistentView
from st3m.ui.view import BaseView, ViewManager


COLORS = [
    (251, 72, 196),  # Pink
    (63, 255, 33),   # Green
    (17, 255, 238),  # Blue
    (254, 80, 0),    # Orange
    (255, 255, 51),  # Yellow
]
COLORS_FLOAT = [(c[0]/255, c[1]/255, c[2]/255) for c in COLORS]

# Number of rounds to win
MAX_ROUNDS = 10
# Length of a note
NOTE_MS = 500
# Length of a pause between notes
PAUSE_MS = 250
# Player timeout per note
TIMEOUT_MS = 2000

STATE_EXPAND = 0
STATE_PLAY = 1
STATE_REPEAT = 2
STATE_END = 3


class Main(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.note = Note()

    def on_enter(self, vm: ViewManager) -> None:
        super().on_enter(vm)
        self.note.stop()
        items = [
            MenuItemLaunchPersistentView('Play!', Memory)
        ]
        menu = SimpleMenu(items=items)
        self.vm.replace(menu)
        # self.vm.replace(Debug())

    def draw(self, ctx: Context) -> None:
        pass


class Memory(BaseView):
    """Memory game mode.

    Plays an expanding sequence of notes that must be repeated by the player.
    """

    def __init__(self) -> None:
        super().__init__()
        self.petals = self.input.captouch.petals[0:9:2]
        self.note = Note()

    def on_enter(self, vm: ViewManager) -> None:
        super().on_enter(vm)
        self.state = STATE_EXPAND
        self.round = 0
        self.sequence = []
        self.color = COLORS[0]

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font = ctx.get_font_name(4)
        ctx.font_size = 60
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.move_to(0, 0)
        ctx.rgb(*self.color).text(str(self.round))

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        # Expand the sequence and fill the queue with notes/pauses
        if self.state == STATE_EXPAND:
            self.color = COLORS_FLOAT[self.round % len(COLORS)]
            self.sequence.append(random.choice(self.petals))
            self.queue = [(None, PAUSE_MS * 3)]
            for petal in self.sequence:
                self.queue.append((petal, NOTE_MS))
                self.queue.append((None, PAUSE_MS))
            self.queue.pop()
            self.state = STATE_PLAY
        # Play queued notes/pauses and fill the queue with expected notes
        elif self.state == STATE_PLAY:
            if self.note.playing:
                self.note.think(ins, delta_ms)
            elif self.queue:
                petal, note_ms = self.queue.pop(0)
                self.note.play(petal, note_ms)
            else:
                self.queue = self.sequence.copy()
                self.timer = 0
                self.state = STATE_REPEAT
        # Wait for the player to repeat the queued notes
        elif self.state == STATE_REPEAT:
            self.timer += delta_ms
            if self.timer > TIMEOUT_MS:
                self.lose()
            elif self.note.playing:
                self.note.think(ins, delta_ms)
            elif self.queue:
                for petal in self.petals:
                    if petal.whole.state == Pressable.PRESSED:
                        expected = self.queue.pop(0)
                        if petal == expected:
                            self.timer = 0
                            self.note.play(petal, -1)
                        else:
                            self.lose()
                        break
            else:
                if self.round + 1 < MAX_ROUNDS:
                    self.round += 1
                    self.state = STATE_EXPAND
                else:
                    self.win()

    def lose(self) -> None:
        self.state = STATE_END
        self.note.stop()
        self.vm.replace(GameEnd('Game Over'))

    def win(self) -> None:
        self.state = STATE_END
        self.note.stop()
        self.vm.replace(GameEnd('You Win!'))


class GameEnd(BaseView):
    """Game end screen.

    Displays a short, rotating message in a random color for 60 seconds.
    """

    def __init__(self, message: str) -> None:
        super().__init__()
        self.message = message
        self.color = random.choice(COLORS_FLOAT)
        self.timer = 0
        self.scale = 1.0
        self.phase = 0.0

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font = ctx.get_font_name(4)
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.move_to(0, 0).scale(self.scale, 1)
        ctx.rgb(*self.color).text(self.message)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.timer += delta_ms
        if self.timer > 60000:
            self.vm.pop()
        self.phase += delta_ms / 1000
        self.scale = math.sin(self.phase)


class Debug(BaseView):
    """Play notes and display the color index for debuging."""
    def __init__(self) -> None:
        super().__init__()
        self.petals = self.input.captouch.petals[0:9:2]
        self.note = Note()
        self.text = None

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font = ctx.get_font_name(4)
        ctx.font_size = 60
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        if self.text is not None:
            ctx.move_to(0, 0)
            ctx.rgb(*self.color).text(str(self.text))

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        if self.note.playing:
            self.note.think(ins, delta_ms)
        else:
            for petal in self.petals:
                if petal.whole.state == Pressable.PRESSED:
                    note_ix = petal.ix // 2
                    self.color = COLORS_FLOAT[note_ix]
                    self.text = note_ix
                    self.note.play(petal, -1)
                    break
            else:
                self.text = None


class Note():
    """Note player.

    Plays notes for individual top petals and lights their LEDs.
    """

    def __init__(self) -> None:
        self.playing = False

    def think(self, ins: InputState, delta_ms: int) -> None:
        if not self.playing:
            return

        if self.time != -1:
            self.time = max(0, self.time - delta_ms)
            if self.time == 0:
                self.stop()
        else:
            if self.petal.whole.state not in [Pressable.DOWN, Pressable.REPEATED]:
                self.stop()

    def play(self, petal: PetalState, note_ms: int) -> None:
        """Play the note for a top petal.

        If petal is None, stay silent and light no LEDs. If note_ms is -1, play
        the petal's note until the petal is released.
        """
        if petal:
            color_ix = petal.ix // 2
            led_ix = 36 + color_ix * 8
            for led_ix in [i % 40 for i in range(led_ix, led_ix + 9)]:
                leds.set_rgb(led_ix, *COLORS[color_ix])
            leds.update()
        self.petal = petal
        self.time = note_ms
        self.playing = True

    def stop(self) -> None:
        """Stop playing the note and clear all LEDs."""
        for led_ix in range(40):
            leds.set_rgb(led_ix, 0, 0, 0)
        leds.update()
        self.petal = None
        self.time = 0
        self.playing = False


# Uncomment when running the application via mpremote
# if __name__ == '__main__':
#     import st3m.run
#     st3m.run.run_view(Main(ApplicationContext()))
